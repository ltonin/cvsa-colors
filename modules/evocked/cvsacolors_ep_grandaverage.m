clc;
clear all;
close all;

subject  = 'a5';
pattern  = 'vacolor';
rootpath = '/mnt/data/Dropbox/Research/cvsa-colors/';

dirpath   = util_getdir(rootpath, subject);
filenames = util_getfile(dirpath, '.gdf', pattern);
NumFiles = length(filenames);

EVENTS_COLORS_ID = [16 17 18];
EVENTS_COLORS_LB = {'green', 'yellow', 'magenta'};
EVENTS_BUTTON_ID = 64;
EVENTS_BUTTON_LB = {'button-press'};
EVENTS_OUT_ID    = 80;
EVENTS_OUT_LB    = {'out'};

NumColors = length(EVENTS_COLORS_ID);

FaultThreshold = 100;
TrialPeriod = [-1 3];
FilterOrder = 4;
FilterBand  = [1 50];
SpatialFilter = 'car'; % 'car';
laplacian = load('lapmask_16ch');

s = []; pos = []; typ = []; Rk = [];

for fId = 1:NumFiles
    cfilename = filenames{fId};
    util_bdisp(['[io] - ' subject ' - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    [currs, currh] = sload(cfilename);
    currs = currs(:, 1:end-1);
    
    % Band-pass filters
    disp(['[proc] - Band pass filter (Order: ' num2str(FilterOrder) ' - Band: ' num2str(FilterBand(1)) '-' num2str(FilterBand(2)) ' Hz)']);     
    currs_bp = filt_bp(currs, FilterOrder, FilterBand, currh.SampleRate);
    
    
    pos = cat(1, pos, currh.EVENT.POS + size(s, 1));
    typ = cat(1, typ, currh.EVENT.TYP);
    s   = cat(1, s, currs_bp);
    Rk  = cat(1, Rk, fId*ones(size(currs, 1), 1));
    
    SampleRate = currh.SampleRate;
end

t = TrialPeriod(1):1/SampleRate:TrialPeriod(2) - 1/SampleRate;
NumChannels = size(s, 2);

%% Data labeling
util_bdisp('[proc] - Data labeling');     
TrialLength = diff(floor(TrialPeriod*SampleRate));
StartPos    = pos(typ == 16 | typ == 17 | typ == 18) + floor(TrialPeriod(1)*SampleRate);
StopPos     = StartPos + TrialLength - 1;

Pk        = typ(typ ==  16 | typ ==  17 | typ ==  18);
NumTrialsTot = length(Pk);

P = zeros(TrialLength, NumChannels, NumTrialsTot);
for trId = 1:NumTrialsTot
    cstart = StartPos(trId);
    cstop  = StopPos(trId);
    P(:, :, trId) = s(cstart:cstop, :);
end

%% Fault trials
util_bdisp('[proc] - Check for fault trials');  
FaultTrials = false(NumTrialsTot, 1);
for trId = 1:NumTrialsTot
    cmax = max(max(P(:, :, trId)));
    if cmax >= FaultThreshold
        FaultTrials(trId) = true;
    end 
end

disp(['[proc] - Number of fault trials removed: ' num2str(sum(FaultTrials))]);
disp(['[proc] - Trials removed: ' num2str(find(FaultTrials)')]);

Prk = Pk(~FaultTrials);
Pr  = P(:, :, ~FaultTrials);

NumTrials = sum(~FaultTrials);

%% Spatial filters
F = zeros(size(Pr));
if strcmpi(SpatialFilter, 'laplacian')
    util_bdisp('[proc] - Spatial filter (Laplacian)'); 
    for trId = 1:NumTrials
        F(:, :, trId) = Pr(:, :, trId)*laplacian.lapmask;
    end
elseif strcmpi(SpatialFilter, 'car')
    util_bdisp('[proc] - Spatial filter (CAR)');     
    for trId = 1:NumTrials
        F(:, :, trId) = proc_car(Pr(:, :, trId));
    end
else
    warning('No spatial filter applied');
    F = Pr;
end

Fk = Prk;

%% Plots
ChanLayout = [0 0 1 0 0; 2 3 4 5 6; 7 8 9 10 11; 12 13 14 15 16];
ChanLabels = {'Cz', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4', 'P3', 'P1', 'Pz', 'P2', 'P4', 'PO3', 'PO1', 'POz', 'PO2', 'PO4'};


figure;
NumRows = 4;
NumCols = 5;

for chId = 1:NumChannels
    [cx, cy] = find(ChanLayout == chId); 
    index = (cx-1)*NumCols + cy;
    subplot(NumRows, NumCols, index);
    
    hold on;
    for cId = 1:NumColors
        cmean = mean(F(:, chId, Fk == EVENTS_COLORS_ID(cId)), 3);
        plot(t, cmean,  EVENTS_COLORS_LB{cId}(1));  
    end
    hold off;
   
    xlim([-0.5 1]);
    ylim([-4 4]);
    plot_vline(0, 'k');
    title(ChanLabels{chId});
    xlabel('Time [s]');
    ylabel('uV');
    grid on;
end

