clc;
clear all;
close all;

subject  = 'a5';
pattern  = 'vacolor';
rootpath = '/mnt/data/Dropbox/Research/cvsa-colors/';

dirpath   = util_getdir(rootpath, subject);
filenames = util_getfile(dirpath, '.gdf', pattern);
NumFiles = length(filenames);

EVENT_GREEN   = 16;
EVENT_YELLOW  = 17;
EVENT_MAGENTA = 18;
EVENTS_COLORS_ID = [EVENT_GREEN EVENT_YELLOW EVENT_MAGENTA];
EVENTS_COLORS_LB = {'green', 'yellow', 'magenta'};
EVENTS_BUTTON_ID = 64;
EVENTS_BUTTON_LB = {'button-press'};
EVENTS_OUT_ID    = 80;
EVENTS_OUT_LB    = {'out'};

ColorPairs(:, 1) = [EVENT_GREEN  EVENT_MAGENTA];
ColorPairs(:, 2) = [EVENT_YELLOW EVENT_MAGENTA];
ColorPairs(:, 3) = [EVENT_GREEN  EVENT_YELLOW];
ColorPairsLb     = {'green-magenta', 'green-yellow', 'yellow-magenta'};


NumColors     = length(EVENTS_COLORS_ID);
NumColorPairs = size(ColorPairs, 2);

FaultThreshold = 100;
TrialPeriod = [0 0.5];
FilterOrder = 4;
FilterBand  = [1 50];
SpatialFilter = 'car'; % 'car';
laplacian = load('lapmask_16ch');

s = []; pos = []; typ = []; Rk = [];

for fId = 1:NumFiles
    cfilename = filenames{fId};
    util_bdisp(['[io] - ' subject ' - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    [currs, currh] = sload(cfilename);
    currs = currs(:, 1:end-1);
    
    % Band-pass filters
    disp(['[proc] - Band pass filter (Order: ' num2str(FilterOrder) ' - Band: ' num2str(FilterBand(1)) '-' num2str(FilterBand(2)) ' Hz)']);     
    currs_bp = filt_bp(currs, FilterOrder, FilterBand, currh.SampleRate);
    
    
    pos = cat(1, pos, currh.EVENT.POS + size(s, 1));
    typ = cat(1, typ, currh.EVENT.TYP);
    s   = cat(1, s, currs_bp);
    Rk  = cat(1, Rk, fId*ones(size(currs, 1), 1));
    
    SampleRate = currh.SampleRate;
end

t = TrialPeriod(1):1/SampleRate:TrialPeriod(2) - 1/SampleRate;
NumChannels = size(s, 2);

%% Data labeling
util_bdisp('[proc] - Data labeling');     
TrialLength = diff(floor(TrialPeriod*SampleRate));
StartPos    = pos(typ == 16 | typ == 17 | typ == 18) + floor(TrialPeriod(1)*SampleRate);
StopPos     = StartPos + TrialLength - 1;

Sk        = typ(typ ==  16 | typ ==  17 | typ ==  18);
NumTrialsTot = length(Sk);

S = zeros(TrialLength, NumChannels, NumTrialsTot);
for trId = 1:NumTrialsTot
    cstart = StartPos(trId);
    cstop  = StopPos(trId);
    S(:, :, trId) = s(cstart:cstop, :);
end

%% Fault trials
util_bdisp('[proc] - Check for fault trials');  
FaultTrials = false(NumTrialsTot, 1);
for trId = 1:NumTrialsTot
    cmax = max(max(S(:, :, trId)));
    if cmax >= FaultThreshold
        FaultTrials(trId) = true;
    end 
end

disp(['[proc] - Number of fault trials removed: ' num2str(sum(FaultTrials))]);
disp(['[proc] - Trials removed: ' num2str(find(FaultTrials)')]);

Dk = Sk(~FaultTrials);
D  = S(:, :, ~FaultTrials);

NumTrials = sum(~FaultTrials);

%% Spatial filters
P = zeros(size(D));
if strcmpi(SpatialFilter, 'laplacian')
    util_bdisp('[proc] - Spatial filter (Laplacian)'); 
    for trId = 1:NumTrials
        P(:, :, trId) = D(:, :, trId)*laplacian.lapmask;
    end
elseif strcmpi(SpatialFilter, 'car')
    util_bdisp('[proc] - Spatial filter (CAR)');     
    for trId = 1:NumTrials
        P(:, :, trId) = proc_car(D(:, :, trId));
    end
else
    warning('No spatial filter applied');
    P = D;
end

Pk = Dk;


%% Features extraction
Param    = {'Peak-Value (PV)', 'Peak-Time (PT)', 'Sum-Value (SV)'};
NumParam = length(Param);

util_bdisp(['[proc] - Features extraction (' num2str(NumParam) ' parameters)']);
for mId = 1:NumParam
    disp(['[proc] - Parameter ' num2str(mId) ': ' Param{mId}]);
end

F  = zeros(NumTrials, NumParam*NumChannels);
Fk = Pk;
for trId = 1:NumTrials
    cdata = P(:, :, trId);
    [cpeak, ctimeid] = max(cdata, [], 1);
    ctime = t(ctimeid);
    csum  = sum(cdata, 1);
    
    F(trId, :) = cat(2, cpeak, ctime, csum); 
end

NumFeatures = NumParam*NumChannels;
ParamId = 1:NumParam;
ParamLb = reshape(repmat(ParamId, NumChannels, 1), NumFeatures, 1);

%% Discriminant power
util_bdisp('[proc] - Computing discriminant power for each color pairs');

DP  = zeros(NumFeatures, NumColorPairs);
DPa = zeros(NumFeatures, NumColorPairs);
for pId = 1:NumColorPairs
    
    % Selecting trial index for current pair
    cpair = ColorPairs(:, pId);
    selindex = Fk == cpair(1) | Fk == cpair(2);
    
    % Computing dp for each parameter separatley
    cdp = [];
    for mId = 1:NumParam
        selparam = ParamLb == ParamId(mId);
        cfeat    = F(selindex, selparam);
        clabel   = Fk(selindex);
        cdp      = cat(1, cdp, cva_tun_opt(cfeat, clabel));
    end
    DP(:, pId) = cdp;
    
    % Computing dp for all parameters
    
    cdpa = cva_tun_opt(F(selindex, :), Fk(selindex));
    DPa(:, pId) = cdpa;
end



%% Features selection

NumSelected = 13;
IdSelected  = cell(NumColorPairs, 1);
IdSelecteda = cell(NumColorPairs, 1);

for pId = 1:NumColorPairs
    cdp = DP(:, pId);
    cdpa = DPa(:, pId);
    
    [~, idsorted] = sort(cdp, 'descend');
    idselected = idsorted(1:NumSelected);
    
    [~, idsorteda] = sort(cdpa, 'descend');
    idselecteda = idsorteda(1:NumSelected);
    
    IdSelected{pId}  = idselected;
    IdSelecteda{pId} = idselecteda;
end

%% Classification
PairOfInterest = 3;
    
PairIndex = Fk == ColorPairs(1, PairOfInterest) | Fk == ColorPairs(2, PairOfInterest);

C  = F(PairIndex, :);
Ck = Fk(PairIndex);


FeaturesId = IdSelecteda{PairOfInterest};

GkTrain = zeros(length(Ck), 1);
GkTest  = zeros(length(Ck), 1);

TrialLb = 1:length(Ck);

for trId = 1:length(Ck)
    TestId  = TrialLb == trId;
    TrainId = ~TestId;
    
   % Train model
    model = classTrain(C(TrainId, FeaturesId), Ck(TrainId), 'lda');

    % Test model
    [~, gktrain] = classTest(model, C(TrainId, FeaturesId));
    [~, gktest]  = classTest(model, C(TestId, FeaturesId));


    GkTrain(TrainId) = gktrain;
    GkTest(TestId) = gktest;
    
end



Accuracy(1) = 100*sum(GkTrain == Ck)./length(Ck);
Accuracy(2) = 100*sum(GkTest == Ck)./length(Ck);

util_bdisp(['[out] - Accuracy on TrainSet: ' num2str(Accuracy(1)) '%']);
util_bdisp(['[out] - Accuracy on TestSet:  ' num2str(Accuracy(2)) '%']);




% 
% 
% for kId = 1:NumFolds
%    
%     TestId  = KIndex == kId;
%     TrainId = ~TestId;
%     
%     % Train model
%     model = classTrain(C(TrainId, FeaturesId), Ck(TrainId), 'qda');
%     
%     % Test model
%     [~, gktrain] = classTest(model, C(TrainId, FeaturesId));
%     [~, gktest]  = classTest(model, C(TestId, FeaturesId));
%     
%     
%     GkTrain(TrainId) = gktrain;
%     GkTest(TestId) = gktest;
%     
% %     rktrain = Ck(TrainId);
% %     rktest  = Ck(TestId);
% %     
% %     % Compute accuracy
% %     Accuracy(kId, 1) = 100*sum(gktrain == rktrain)./length(rktrain);
% %     Accuracy(kId, 2) = 100*sum(gktest == rktest)./length(rktest);
% end
% 
% Accuracy(1) = 100*sum(GkTrain == Ck)./length(Ck);
% Accuracy(2) = 100*sum(GkTest == Ck)./length(Ck);
% 
% util_bdisp(['[out] - Accuracy on TrainSet: ' num2str(Accuracy(1)) '%']);
% util_bdisp(['[out] - Accuracy on TestSet:  ' num2str(Accuracy(2)) '%']);

%% Plots
